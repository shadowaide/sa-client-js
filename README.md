### Javascript client for ShadowAide NPS Dashboard

# What is ShadowAide?
- **Net Promoter Score** - Net Promoter Score is a simple way to guage your customer's satisfaction
and get feedback on your app that you can use to reduce churn and increase user recommendations
- **Feedback** - Bypass the need to manually reach out to users for feedback.  ShadowAide automatically
surveys your users on a schedule to keep the feedback coming in, while limiting survey fatigue.
- **Simple** - ShadowAide makes adding NPS surveys into your app simple.  Add the script
to the page(s) you want to show the survey and we take care of sampling the users for feedback

# Installation

```bash
npm install shadowaide-js --save
```

# Usage

```js
    <script src="js/shadowaide.js" data-apikey="<insert key here>"></script>
    <script type="text/javascript">shadowaide.showSurvey(); </script>
```

# Documentation
For additional info including API docs, recipes, etc. check out [our docs](https://www.shadowaide.com/docs/index.html)!

# Need a ShadowAide Account?
Head over to [ShadowAide](https://www.shadowaide.com) to signup and get started!