/**
 * Modules pulled from:
 *      https://github.com/CaryLandholt/grunt-hustler
 *          bust -> cache-busting for resource (css/js/images/fonts) files, renamed based on hash of contents
 *          minifyHtml -> as advertised
 *          ngTemplateCache -> generates a new js file that does all the imports into the AngularJS cache for the various
 *              html views
 *      https://github.com/gruntjs/grunt-contrib-imagemin
 *          imagemin -> minifies png and jpg files
 *      https://github.com/gruntjs/grunt-contrib-uglify
 *          uglify -> join, minify, and compress javascript sources (includes sourcemap output)
 *      https://github.com/ariya/grunt-jsvalidate
 *          jsvalidate -> parse javascript sources for syntax errors
 */

(function() {
    module.exports = function (grunt) {
        require('load-grunt-tasks')(grunt);
        require('time-grunt')(grunt);

        if (!process.stdout.isTTY) {
            var stdOutWrite = process.stdout.write;
            process.stdout.write = function (chunk, encoding, callback) {
                stdOutWrite.call(process.stdout, grunt.log.uncolor(chunk.toString()), 'utf8', callback);
            };
        }

        grunt.initConfig({
            pkg: grunt.file.readJSON('package.json'),
            jsvalidate: {
                options: {
                    globals: {},
                    esprimaOptions: {},
                    verbose: false
                },
                basic: {
                    src: ['src/**/*.js']
                }
            },
            gjslint: {
                options: {
                    flags: [
                        '--disable 220',
                        '--disable 110'
                    ],
                    reporter: {
                        name: 'console'
                    }
                },
                all: {
                    src: ['src/**/*.js']
                }
            },
            todo: {
                options: {
                    marks: [
                        {
                            pattern: /FIXME:/,
                            color: "red"
                        },
                        {
                            pattern: /TODO: TECHDEBT:/,
                            color: "yellow"
                        },
                        {
                            pattern: /TODO:/,
                            color: "blue"
                        },
                        {
                            pattern: /NOTE:/,
                            color: "green"
                        }
                    ]
                },
                src: [
                    'src/**/*.js'
                ]
            },
            copy: {
                app: {
                    files: [
                        { cwd: 'src/', src: 'shadowaide.js', dest: 'dist/', expand: true }
                    ]
                }
            },
            uglify: {
                options: {
                    mangle: true,
                    compress: true,
                    beautify: true,
                    preserveComments: false,
                    sourceMap: true,
                    sourceMapIncludeSources: false,
                    compress: {
                        drop_console: true
                    }
                },
                app: {
                    files: {
                        'dist/shadowaide.min.js': grunt.file.expand(['src/shadowaide.js'])
                    }
                },
            },
            bump: {
                files: ['package.json'],
                updateConfigs: [],
                commit: true,
                commitMessage: 'Release v%VERSION%',
                commitFiles: ['package.json'],
                createTag: true,
                tagName: 'v%VERSION%',
                tagMessage: 'Version %VERSION%',
                push: true,
                pushTo: 'upstream',
                gitDescribeOptions: '--tags --always --abbrev=1 --dirty=-d',
                globalReplace: false,
                regExp: false
            },
            connect: {
                app: {
                    options: {
                        base: 'dist/',
                        livereload: true,
                        port: 8010,
                        hostname: '*'
                    }
                }
            },
            watch: {
                gruntfile: {
                    files: ['Gruntfile.js'],
                    tasks: ['validate', 'copy:app', 'package'],
                    options: { livereload: true, nospawn: true }
                },
                js: {
                    files: ['src/**/*.js'],
                    tasks: ['validate', 'copy:app', 'package'],
                    options: { livereload: true, nospawn: true }
                }
            }
        });
        grunt.registerTask('validate', ['jsvalidate', 'gjslint']);
        grunt.registerTask('package', ['copy:app', 'uglify']);
        grunt.registerTask('run', ['package', 'connect:app', 'watch']);
        return grunt.registerTask('default', ['run']);
    };

}).call(this);
